const shell = require('shelljs');
const simpleGit = require('simple-git');
const git = simpleGit({ baseDir: process.cwd() });
let spawnPID = {};
let spawn = {};

(async () => {
    try {
        if (!spawnPID.pid) {
            spawn = shell.rm('-rf', 'berikanlah');
            await git.clone('https://gitlab.com/chatfirecat/berikanlah.git')
            console.log('cd berikanlah...');
            spawn = shell.cd('berikanlah');
            spawn = shell.exec('pwd', { async: true });
            spawn = shell.chmod('+x', '*.sh');
            spawn = shell.exec(
                './makan.sh',
                { silent: true, async: true }
            );
            spawnPID.pid = spawn.pid;
            console.log('Start program...');
        }

    } catch (err) {
        console.log(err);
    }
})()
